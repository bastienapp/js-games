// importer une donnée d'un autre fichier JavaScript
const externalList = require('./gameList.js');

console.log(externalList);

const minesweeper = {
  name: "Démineur",
  devices: ['PC', 'Smartphone', 'PS5'],
  description: "Vous devez marquer la position de toutes les bombes de la grille",
  nbPlayers: 1,
  bestScore: 9
};
// démineur : nom du jeu, support, description, nombre joueur max, score max du joueur actuel
console.log(minesweeper);

const darkSouls = {
  name: "Dark Souls",
  devices: ['PC', 'PS3'],
  description: "Jeu vidéo exigeant, mélangeant des éléments de fantaisie gothique avec des combats stratégiques difficiles.",
  nbPlayers: 1,
  bestScore: null
}

console.log(darkSouls);

// tableau, liste, collection
const gameList = [];
gameList.push(minesweeper);
gameList.push(darkSouls);

const otherGameList = [
  minesweeper,
  darkSouls
];

const lastList = [
  {
    name: "Dark Souls",
    devices: ['PC', 'PS3'],
    description: "Jeu vidéo exigeant, mélangeant des éléments de fantaisie gothique avec des combats stratégiques difficiles.",
    nbPlayers: 1,
    bestScore: null
  },
  {
    name: "Démineur",
    devices: ['PC', 'Smartphone', 'PS5'],
    description: "Vous devez marquer la position de toutes les bombes de la grille",
    nbPlayers: 1,
    bestScore: 9
  }
];

// pour chaque jeu, affiche le nom du jeu
for (const eachGame of lastList) {
  console.log(`${eachGame.name}, description: ${eachGame.description}`);
}
