//       index  0  1  2   3    4!  notes.length = 4
const notes = [12, 8, 16, 16];
console.log(typeof notes);
console.log(notes[0]);
console.log(notes[3]);
console.log(notes[notes.length - 1]);

// .length : méthode ? propriété ! (pas de parenthèse)
// boucle pour parcourir

// for : POUR
// tracer : passer soit même à la main dans la boucle
for (let i = 0; i < notes.length; i++) { // const notes = [12, 8, 16, 16];
  const currentValue = notes[i]; // notes[0] > 12, notes[1] > 8 ...
  console.log("valeur: ", currentValue, " à l'index ", i);
}

let nameList = ["Michel", "Brenda", "Joe"];
// for .. of : POUR CHAQUE
for (let name of nameList) { // for (let element of array)
  console.log("valeur: ", name);
}

// nameList[] = "value" : pas possible en JS !
nameList.push("Harry Potter"); // ajouter un élément à la fin du tableau
nameList.unshift("Hermione Granger"); // ajouter un élément au début du tableau
console.log(nameList);
// pop() : retirer à la fin
// shift() : retirer au début

const allValues = nameList.join(";");
const cars = "Honda-Skoda-Fiat";
const carList = cars.split("-");
console.log(carList);