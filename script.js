/*
types :

string
number
boolean
undefined

null (eut-être object ?)

array : object
*/

const lastName = "Durand";
const firstName = "Michel";
const age = 50;

// objet littéral
const janeDoe = {
  // propriété (attribut): valeur
  firstName: "Jane",
  lastName: "Doe",
  age: 30,
  // méthode: fonction dans un objet
  introduceMyself() {
    console.log(`My name is ${this.firstName} ${this.lastName}`);
  }
};

// méthode
janeDoe.introduceMyself();

// référence vers un objet (l'instance)
const other = janeDoe;
janeDoe.age = 24;

// console.log(janeDoe.age, other.age);

let a = 3;
let b = a;
a = 12;
// console.log(a, b);

/*
class Person {

  constructor(firstName, lastName, age) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
  }

  sayHello() {
    console.log("Hello my name is " + this.firstName)
  }
}

const michel = new Person("Michel", "Durand", 50);
const brenda = new Person("Brenda", "Dupond", 20);

michel.sayHello();
brenda.sayHello();
*/
// soit submit sur formulaire
// soit click sur le bouton
const studentList = [];

// récupérer le bouton
const buttonSend = document.querySelector("#person-submit");
// écouter l'action au clic sur le bouton
buttonSend.addEventListener('click', function(event) {
  // empêcher l'action par défaut (rafraîchir la page -> l'envoi du formulaire)
  event.preventDefault();

  // récupérer le formulaire
  const personForm = document.querySelector("#person-form");

  // récupérer les valeurs du formulaire (nom, prénom, age)
  const lastNameValue = personForm.personLastname.value;
  const firstNameValue = personForm.personFirstname.value;
  const ageValue = personForm.personAge.value;
  // console.log("form values:", lastNameValue, firstNameValue, ageValue);

  // créer un objet littéral avec ces valeurs
  // student
  const student = {
    lastName: lastNameValue,
    firstName: firstNameValue,
    age: ageValue,
  }

  // afficher le contenu dans la console
  console.log("student", student);
});